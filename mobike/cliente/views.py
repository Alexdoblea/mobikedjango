from django.shortcuts import render, redirect, HttpResponse
from cliente.forms import *


def inicio(request):
    return render(request, 'inicio.html')

def registrar_usuario(request):
    validador=False
    existe=False
    if request.method=='POST':
        form = ClienteFormulario(data=request.POST)
        email = request.POST.get('email')

        cliente_existe = Cliente.objects.filter(email=email)
        if cliente_existe:
            existe = True
            contexto = {'form': form,
                        'existe':existe}
            return render(request, 'cliente/registrarse.html', contexto)
        else:
            if form.is_valid():
                form.save()
                validador = True
                contexto = {'form': form,
                            'validador':validador}
                return render(request, 'cliente/registrarse.html', contexto)
        
    form = ClienteFormulario
    contexto = {'form':form}
    return render(request, 'cliente/registrarse.html', contexto)

def iniciar_sesion(request):
    return render(request, 'cliente/iniciar_sesion.html')

def perfil(request):
    if request.method=='POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        existe = Cliente.objects.filter(email=email).filter(password=password)
        if existe:
            cliente=existe.get()
            contexto = {'cliente':cliente}
            return render(request, 'cliente/perfil.html', contexto)
        else:
            contexto={'no_existe':True}
            return render(request, 'cliente/iniciar_sesion.html', contexto)
    return render(request, 'cliente/perfil.html')

def editar_datos(request, id):
    if request.method=='POST':
        email=request.POST.get('email')
        nombre=request.POST.get('nombre')
        apellido=request.POST.get('apellido')
        comuna=request.POST.get('comuna')
        direccion=request.POST.get('direccion')
        cliente=Cliente.objects.filter(id=id)
        cliente_perfil=Cliente.objects.filter(id=id).get
        cliente.update(email=email,
                        nombre=nombre,
                        apellido=apellido,
                        comuna=comuna,
                        direccion=direccion)
        contexto={'datos_modificados':True,
                  'cliente':cliente_perfil}
        return render(request, 'cliente/perfil.html', contexto)
    return redirect('cliente:perfil')

def cambiar_tarjeta(request, id):
    if request.method=='POST':
        tarjeta_numero=request.POST.get('tarjeta_numero')
        tarjeta_mes=request.POST.get('tarjeta_mes')
        tarjeta_anio=request.POST.get('tarjeta_anio')
        cliente=Cliente.objects.filter(id=id)
        cliente_perfil=Cliente.objects.filter(id=id).get
        cliente.update(tarjeta_numero=tarjeta_numero,
                        tarjeta_mes=tarjeta_mes,
                        tarjeta_anio=tarjeta_anio)
        contexto={'tarjeta_modificada':True,
                  'cliente':cliente_perfil}
        return render(request, 'cliente/perfil.html', contexto)
    return redirect('cliente:perfil')

def cambiar_contrasena(request, id):
    if request.method=='POST':
        passwordActual=request.POST.get('password')
        cliente_perfil=Cliente.objects.filter(id=id).get()
        valido=Cliente.objects.filter(id=id).filter(password=passwordActual)
        if valido:
            cliente=valido
            password_nueva=request.POST.get('password_nueva')
            password_nueva2=request.POST.get('password_nueva2')
            if password_nueva != password_nueva2:
                contexto={'password_valido':False,
                          'cliente':cliente_perfil}
                return render(request, 'cliente/perfil.html', contexto)
            else:
                cliente.update(password=password_nueva)
                contexto={'password_valido':True,
                          'cliente':cliente_perfil}
                return render(request, 'cliente/perfil.html', contexto)
        else:
            contexto={'password_correcto':False,
                      'cliente':cliente_perfil}
            return render(request, 'cliente/perfil.html', contexto)
    return redirect('cliente:perfil')

def cerrar_sesion(request):
    contexto={'sesion_cerrada':True}
    return render(request, 'inicio.html', contexto)





