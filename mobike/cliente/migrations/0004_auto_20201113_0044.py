# Generated by Django 3.1.3 on 2020-11-13 03:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0003_auto_20201112_2026'),
    ]

    operations = [
        migrations.AddField(
            model_name='cliente',
            name='id',
            field=models.AutoField(default=1, primary_key=True, serialize=False),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='cliente',
            name='email',
            field=models.EmailField(max_length=30, unique=True),
        ),
    ]
