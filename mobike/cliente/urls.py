from django.urls import path
from cliente.views import *

app_name='cliente'

urlpatterns = [
    path('', inicio, name='inicio'),
    path('registrar_usuario/', registrar_usuario, name='registrar_usuario'),
    path('iniciar_sesion/', iniciar_sesion, name='iniciar_sesion'),
    path('perfil/', perfil, name="perfil"),
    path('editar_datos/<int:id>', editar_datos, name="editar_datos"),
    path('cambiar_tarjeta/<int:id>', cambiar_tarjeta, name="cambiar_tarjeta"),
    path('cambiar_contrasena/<int:id>', cambiar_contrasena, name="cambiar_contrasena"),
    path('cerrar_sesion/', cerrar_sesion, name="cerrar_sesion"),

]