from django import forms
from cliente.models import *

class ClienteFormulario(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = ['email', 'password', 'nombre', 'apellido', 'comuna', 'direccion','tarjeta_numero', 'tarjeta_mes', 'tarjeta_anio', 'tarjeta_codigo']

class ClienteIniciarSesion(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = ['email','password']