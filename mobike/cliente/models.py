from django.db import models

class Cliente(models.Model):
    id=models.AutoField(primary_key=True)
    email=models.EmailField(max_length=30, unique=True)
    password=models.CharField(max_length=30)
    nombre=models.CharField(max_length=30)
    apellido=models.CharField(max_length=30)
    comuna=models.CharField(max_length=30)
    direccion=models.CharField(max_length=60)
    tarjeta_numero=models.IntegerField()
    tarjeta_mes=models.IntegerField()
    tarjeta_anio=models.IntegerField()
    tarjeta_codigo=models.IntegerField()

