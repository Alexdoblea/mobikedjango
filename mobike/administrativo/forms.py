from django import forms
from administrativo.models import *

class AdministrativoFormulario(forms.ModelForm):
    class Meta:
        model = Administrativo
        fields = ['email', 'password', 'nombre', 'apellido']

class AdministrativoIniciarSesion(forms.ModelForm):
    class Meta:
        model = Administrativo
        fields = ['email','password']