from django.shortcuts import render, HttpResponse, redirect, HttpResponseRedirect
from cliente.models import *
from administrativo.forms import *
from cliente.forms import *



def iniciar_sesion(request):
    return render(request, 'administrativo/iniciar_sesion.html')

def perfil(request):
    if request.method=='POST':
        email = request.POST.get('email')
        password= request.POST.get('password')
        existe = Administrativo.objects.filter(email=email).filter(password=password)
        if existe:
            administrativo=existe.get()
            contexto={'administrativo':administrativo}
            return render(request, 'administrativo/perfil.html', contexto)
        else:
            contexto={'no_existe':True}
            return render(request, 'administrativo/iniciar_sesion.html', contexto)
    return render(request, 'administrativo/perfil.html')

def administrador_usuarios(request):
    clientes = Cliente.objects.all()
    contexto = {'clientes':clientes}
    if request.method=='POST':
        return render(request, 'administrativo/administrador_usuarios.html', contexto)
    return render(request, 'administrativo/administrador_usuarios.html', contexto)
    

def eliminar_cliente(request, id):
    cliente = Cliente.objects.get(id=id)
    clientes = Cliente.objects.all()
    cliente.delete()
    validador_borrado=True
    contexto={'validador_borrado':validador_borrado,
              'clientes':clientes}
    return render(request, 'administrativo/administrador_usuarios.html', contexto)

def editar_usuario(request, id):
    if request.method=='POST':
        email=request.POST.get('email')
        nombre=request.POST.get('nombre')
        apellido=request.POST.get('apellido')
        comuna=request.POST.get('comuna')
        direccion=request.POST.get('direccion')
        cliente = Cliente.objects.filter(id=id)
        cliente.update(email=email, nombre=nombre, apellido=apellido, comuna=comuna, direccion=direccion)
        validador_actualizar=True
        contexto={'validador_actualizar':validador_actualizar}
        return redirect('administrativo:administrador_usuarios')
    return redirect('administrativo:administrador_usuarios')

def buscar_id(request):
    if request.method=='POST':
        idBuscar=request.POST.get('idBuscar')
        existe=Cliente.objects.filter(id=idBuscar)
        if existe:
            cliente = existe.get()
            contexto={'cliente':cliente,
                      'existe':True}
            return render(request, 'administrativo/buscar_id.html', contexto)
        else:
            contexto={'existe':False,
                      'idBuscado':idBuscar}
            return render(request, 'administrativo/buscar_id.html', contexto)

    return render(request, 'administrativo/buscar_id.html')

def registrar_usuario(request):
    validador=False
    existe=False
    if request.method=='POST':
        form = ClienteFormulario(data=request.POST)
        email = request.POST.get('email')

        cliente_existe = Cliente.objects.filter(email=email)
        if cliente_existe:
            existe = True
            contexto = {'existe':existe}
            return render(request, 'administrativo/perfil.html', contexto)
        else:
            if form.is_valid():
                form.save()
                validador = True
                contexto = {'validador':validador}
                return render(request, 'administrativo/perfil.html', contexto)
                return redirect()
    return render(request, 'administrativo/perfil.html')

def registrar_administrativo(request):
    validador=False
    existe=False
    if request.method=='POST':
        form = AdministrativoFormulario(data=request.POST)
        email = request.POST.get('email')

        administrativo_existe = Administrativo.objects.filter(email=email)
        if administrativo_existe:
            existe = True
            contexto = {'existe_administrativo':existe}
            return render(request, 'administrativo/perfil.html', contexto)
        else:
            if form.is_valid():
                form.save()
                validador = True
                contexto = {'validador_administrativo':validador}
                return render(request, 'administrativo/perfil.html', contexto)
                return redirect()
    return render(request, 'administrativo/perfil.html')

def ver_administrativos(request):
    administrativos = Administrativo.objects.all()
    contexto = {'administrativos':administrativos}
    if request.method=='POST':
        return render(request, 'administrativo/ver_administrativos.html', contexto)
    return render(request, 'administrativo/ver_administrativos.html', contexto)

def cerrar_sesion(request):
    contexto={'sesion_cerrada':True}
    return render(request, 'inicio.html', contexto)
