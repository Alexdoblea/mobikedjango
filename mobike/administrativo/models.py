from django.db import models

class Administrativo(models.Model):
    id=models.AutoField(primary_key=True)
    email=models.EmailField(max_length=30, unique=True)
    password=models.CharField(max_length=30)
    nombre=models.CharField(max_length=30)
    apellido=models.CharField(max_length=30)

