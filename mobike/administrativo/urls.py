from django.urls import path
from administrativo.views import *

app_name='administrativo'

urlpatterns = [
    path('administrador_usuarios/', administrador_usuarios, name='administrador_usuarios'),
    path('iniciar_sesion/', iniciar_sesion, name='iniciar_sesion'),
    path('perfil/', perfil, name="perfil"),
    path('administracion_usuarios/', administrador_usuarios, name='administracion_usuarios'),
    path('eliminar_cliente/<int:id>', eliminar_cliente, name='eliminar_cliente'),
    path('editar_usuario/<int:id>', editar_usuario, name='editar_usuario'),
    path('buscar_id/', buscar_id, name='buscar_id'),
    path('registrar_usuario/', registrar_usuario, name="registrar_usuario"),
    path('registrar_administrativo/', registrar_administrativo, name="registrar_administrativo"),
    path('ver_administrativos/', ver_administrativos, name="ver_administrativos"),
    path('cerrar_sesion/', cerrar_sesion, name="cerrar_sesion"),
]